import axios from 'axios'

export default {
  logIn(credential){
    // return axios.post(process.env.VUE_APP_API_URL + '/login', credential)
    return axios.post('http://government.tuongdev.software/api/login', credential)
  },
  register(credential) {    
    // return axios.post(process.env.VUE_APP_API_URL + '/register', credential)
    return axios.post('http://government.tuongdev.software/api/register', credential)
  },
  getUserById(id){
    // return axios.get(process.env.VUE_APP_API_URL + '/user/' + id)
    return axios.get('http://government.tuongdev.software/api/user/' + id)
  }
}