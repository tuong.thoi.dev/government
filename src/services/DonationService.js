import axios from 'axios'

export default {
  getDonations(){
    // return axios.get(process.env.VUE_APP_API_URL + '/donation/getall')
    return axios.get('http://government.tuongdev.software/api/donation/getall')
  },
  getDonationById(id){
    // return axios.get(process.env.VUE_APP_API_URL + '/donation/getbyid/' + id)
    return axios.get('http://government.tuongdev.software/api/donation/getbyid/' + id)
  }
}