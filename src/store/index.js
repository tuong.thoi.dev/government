import { createStore } from "vuex";


export default createStore({
  state: {
    token: null,
    user: null,
    isAuthenticated: false
  },

  getters: {
    isAuthenticated: (state) => {
      return state.isAuthenticated
    },
    getToken: (state) => {
      return state.token
    },
    getUser: (state) => {
      return state.user
    }
  },

  mutations: {    
    SET_AUTH(state, sign) {
      state.isAuthenticated = sign
    },
    SET_TOKEN(state, token) {
      sessionStorage.setItem('authToken', token)
      console.log(sessionStorage.getItem('authToken'))
      state.token = token
    },
    SET_USER(state, data) {
      sessionStorage.setItem('authUser', JSON.stringify(data))    
      console.log(sessionStorage.getItem('authUser'))
      state.user = data
    },
    CLEAR_AUTH(state) {
      state.token = null
      state.user = null
      state.isAuthenticated = false
      sessionStorage.removeItem('authUser')
      sessionStorage.removeItem('authToken')
    },
    CHECK_AUTH(state){
      if(sessionStorage.getItem('authUser') && sessionStorage.getItem('authToken')) {
        state.user = JSON.parse(sessionStorage.getItem('authUser'))
        state.token = sessionStorage.getItem('authToken')
        state.isAuthenticated = true
      }
      else state.isAuthenticated = false
        
    }
  },

  actions: {
    logIn({ commit }, data) {
      commit('SET_USER', data)
      commit('SET_TOKEN', data.token_access)
      commit('SET_AUTH', true)
    },

    register({ commit }, data) {
      commit('SET_USER', data)
      commit('SET_TOKEN', data.token_access)
      commit('SET_AUTH', true)
    },

    setUser({commit}, data){
      commit('SET_USER', data)
    },

    logOut({ commit }) {
      commit('CLEAR_AUTH')
    },

    checkAuth({commit}){
      commit('CHECK_AUTH')
    }
  }
});
