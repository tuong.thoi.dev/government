import axios from 'axios'
import store from '../store/index.js'

export default {
  postDonations(donation) {
    let config = {
      headers: {
        Authorization: `Bearer ${store.getters.getToken}`
      }
    }
    // return axios.post(process.env.VUE_APP_API_URL + '/donationuser', donation, config)
    return axios.post('http://government.tuongdev.software/api/donationuser', donation, config)
  },
  postPayment(payment) {
    let config = {
      headers: {
        Authorization: `Bearer ${store.getters.getToken}`
      }
    }
    // return axios.post(process.env.VUE_APP_API_URL + '/payment', payment, config)
    return axios.post('http://government.tuongdev.software/api/payment', payment, config)
  },
  getRecentPayment(id){
    let config = {
      headers: {
        Authorization: `Bearer ${store.getters.getToken}`
      }
    }
    // return axios.get(process.env.VUE_APP_API_URL + '/donationuser/donation/top10/' + id, config)
    return axios.get('http://government.tuongdev.software/api/donationuser/donation/top10/' + id, config)
  }
}