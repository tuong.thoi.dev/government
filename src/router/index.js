import { createRouter, createWebHistory } from "vue-router"
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Donation from '../views/Donation.vue'
import Payment from '../views/Payment.vue'
import NotFound from '../views/NotFound.vue'
import store from '../store/index.js'


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: 'Trang chủ'
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      title: 'Đăng nhập'
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      title: 'Đăng ký'
    }
  },
  {
    path: '/donation/:id',
    name: 'Donation',
    component: Donation,
    meta: {
      title: 'Đóng góp'
    }
  },
  {
    path: '/payment/:id',
    name: 'Payment',
    component: Payment,
    meta: {
      title: 'Thanh toán',
      requireAuth: true
    }
  },
  { 
    path: '/:pathMatch(.*)', 
    component: NotFound,
    meta: {
      title: 'Trang không tồn tại'
    }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach(async (to, from, next) => {
  store.dispatch('checkAuth')
  if(to.matched.some(record => record.meta.requireAuth)) {
    if(!store.getters.isAuthenticated){
      next({ name: 'Login', query: {link: from.fullPath}})
    }
    else next()
  }
  else next()
})

const DEFAULT_TITLE = 'Gov Donate'
router.afterEach((to) => {
  document.title = (to.meta.title + ' - ' + DEFAULT_TITLE) || DEFAULT_TITLE;
});

export default router;
